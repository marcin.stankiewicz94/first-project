import { Country } from './country';

export class User {
    id: string;
    name: string;
    surname: string;
    age: number;
    country: Country;
    email: string;
}
