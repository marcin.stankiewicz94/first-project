import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';
import { User } from '../user';
import { Observable, Subscription } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css']
})
export class HomepageComponent implements OnInit {

  addSubscription: Subscription;
  users$: Observable<Array<User>>;
  users: Array<User>;
  ageFrom = '';
  ageTo = '';

  checkAgeForm = this.fb.group({
    ageFrom: [''],
    ageTo: ['']
  });

  constructor(private userService: UserService,
              private router: Router,
              private route: ActivatedRoute,
              private fb: FormBuilder) { }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      if (params.country) {
        const country = params.country;
        this.users$ = this.userService.getUsers();
        this.users$.subscribe(users => this.users = users.filter(user => (user.country === country.toUpperCase())));
      }
      else if (params.search) {
        this.users$ = this.userService.findUsersByParameter(params.search);
        this.users$.subscribe(users => this.users = users);
      }
      else {
        this.users$ = this.userService.getUsers();
        this.users$.subscribe(users => this.users = users);
      }
    });
  }

  onSubmit(value: { ageFrom: number; ageTo: number; }): void {
    // this.users$ = this.userService.getUsers();
    // this.users$.subscribe(users => this.users = users.filter(user => (user.age >= value.ageFrom) && (user.age <= value.ageTo)));
    this.users = this.users.filter(user => (user.age >= value.ageFrom) && (user.age <= value.ageTo));
  }

  deleteUser(id: string): void {
    if (window.confirm('Are you sure you want to delete this item?')){
      this.userService.deleteUser(id).subscribe(() => location.reload());
    }
  }

  sortUsersByNameAsc(): void {
    this.users = this.userService.sortUsersByNameAsc(this.users);
  }

  sortUsersByNameDesc(): void {
    this.users = this.userService.sortUsersByNameDesc(this.users);
  }

  sortUsersBySurnameAsc(): void {
    this.users = this.userService.sortUsersBySurnameAsc(this.users);
  }

  sortUsersBySurnameDesc(): void {
    this.users = this.userService.sortUsersBySurnameDesc(this.users);
  }

  sortUsersByAgeAsc(): void {
    this.users = this.userService.sortUsersByAgeAsc(this.users);
  }

  sortUsersByAgeDesc(): void {
    this.users = this.userService.sortUsersByAgeDesc(this.users);
  }

  // tslint:disable-next-line:use-lifecycle-interface
  ngOnDestroy() {
    if (this.addSubscription){
      this.addSubscription.unsubscribe();
    }
  }
}
