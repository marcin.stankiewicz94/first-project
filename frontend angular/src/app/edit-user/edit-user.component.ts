import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { UserService } from '../services/user.service';
import { Router, ActivatedRoute } from '@angular/router';
import { User } from '../user';
import { Subscription } from 'rxjs/internal/Subscription';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.css']
})
export class EditUserComponent implements OnInit {

  addSubscription: Subscription;
  id = '';
  user: User;

  checkIdForm = this.fb.group({
    id: ['']
  });

  editUserForm = this.fb.group({
    login: [''],
    password: [''],
    age: [''],
    email: ['']
  });

  constructor(private userService: UserService,
              private router: Router,
              private route: ActivatedRoute,
              private fb: FormBuilder) { }

  ngOnInit(): void {
    this.editUserForm.disable();
    document.getElementById('addButton').setAttribute('disabled', 'disabled');
    this.autoCompletionDataInForm();
  }

  private autoCompletionDataInForm() {
    this.route.params.subscribe(params => {
      if (params.id) {
        this.userService.getUser(params.id).subscribe(use => {
          this.id = params.id;
          this.user = use;
          document.getElementById('checkId').style.display = 'none';
          this.editUserForm.enable();
          document.getElementById('addButton').removeAttribute('disabled');
        });
      }
      else {
        document.getElementById('checkId').style.display = 'ok';
      }
    });
  }

  onSubmitCheck(value): void {
    this.userService.getUser(value.id).subscribe(use => {
      this.id = value.id;
      this.user = use;
      if (value.id){
        this.editUserForm.enable();
        document.getElementById('addButton').removeAttribute('disabled');
      }
    });
  }

  onSubmit(value): void {
    const user = new User();
    if (value.login.length > 0){
      user.name = value.login;
    }
    else {
      user.name = this.user.name;
    }

    if (value.password.length > 0){
      user.surname = value.password;
    }
    else {
      user.surname = this.user.surname;
    }

    if (value.age > 0){
      user.age = value.age;
    }
    else {
      user.age = this.user.age;
    }

    if (value.country){
      user.country = value.country;
    }
    else {
      user.country = this.user.country;
    }

    if (value.email.length > 0){
      user.email = value.email;
    }
    else {
      user.email = this.user.email;
    }
    document.getElementById('addButton').setAttribute('disabled', 'disabled');
    this.userService.editUser(this.id, user).subscribe(editeduser => {
      this.router.navigate(['/quarkus']);
    });
  }

  // tslint:disable-next-line:use-lifecycle-interface
  ngOnDestroy() {
    if (this.addSubscription){
      this.addSubscription.unsubscribe();
    }
  }
}
