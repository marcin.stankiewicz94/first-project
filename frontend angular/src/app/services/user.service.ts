import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../user';
import { Observable } from 'rxjs';

import { catchError, map } from 'rxjs/operators';
import { throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  usersURL = 'http://localhost:8080/users';

  constructor(
    private http: HttpClient) { }

  handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
        // client-side error
        errorMessage = `Error: ${error.error.message}`;
    } else {
        // server-side error
        errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    return throwError(errorMessage);
  }

  getUser(id: string): Observable<User> {
    return this.http.get<User>(this.usersURL + '/' + id).pipe(
      catchError(this.handleError)
    );
  }

  getUsers(): Observable<Array<User>> {
    return this.http.get<Array<User>>(this.usersURL);
  }

  addUser(user: User): Observable<User> {
    return this.http.post<User>(this.usersURL, user).pipe(
      catchError(this.handleError)
    );
  }

  editUser(id: string, user: User): Observable<User> {
    // tslint:disable-next-line:prefer-template
    return this.http.put<User>(this.usersURL + '/' + id, user);
  }

  deleteUser(id: string): Observable<void>{
    return this.http.delete<void>(this.usersURL + '/' + id).pipe(
      catchError(this.handleError)
    );
  }

  findUsersByParameter(param: string): Observable<Array<User>> {
    return this.http.get<Array<User>>(this.usersURL + '/find/' + param);
  }

  sortUsersByNameAsc(allUsers: Array<User>): Array<User> {
    return allUsers.sort((user1, user2) => this.sortByNameAsc(user1, user2));
  }

  sortUsersByNameDesc(allUsers: Array<User>): Array<User> {
    return allUsers.sort((user1, user2) => this.sortByNameDesc(user1, user2));
  }

  sortUsersBySurnameAsc(allUsers: Array<User>): Array<User> {
    return allUsers.sort((user1, user2) => this.sortBySurnameAsc(user1, user2));
  }

  sortUsersBySurnameDesc(allUsers: Array<User>): Array<User> {
    return allUsers.sort((user1, user2) => this.sortBySurnameDesc(user1, user2));
  }

  sortUsersByAgeAsc(allUsers: Array<User>): Array<User> {
    return allUsers.sort((user1, user2) => this.sortByAgeAsc(user1, user2));
  }

  sortUsersByAgeDesc(allUsers: Array<User>): Array<User> {
    return allUsers.sort((user1, user2) => this.sortByAgeDesc(user1, user2));
  }

  private sortByNameAsc(user1: User, user2: User): number {
    if (user1.name < user2.name) {
      return -1; }
    if (user1.name > user2.name) {
      return 1; }
  }

  private sortByNameDesc(user1: User, user2: User): number {
    if (user1.name < user2.name) {
      return 1; }
    if (user1.name > user2.name) {
      return -1; }
  }

  private sortBySurnameAsc(user1: User, user2: User): number {
    if (user1.surname < user2.surname) {
      return -1; }
    if (user1.surname > user2.surname) {
      return 1; }
  }

  private sortBySurnameDesc(user1: User, user2: User): number {
    if (user1.surname < user2.surname) {
      return 1; }
    if (user1.surname > user2.surname) {
      return -1; }
  }

  private sortByAgeAsc(user1: User, user2: User): number {
    if (user1.age < user2.age) {
      return -1; }
    if (user1.age > user2.age) {
      return 1; }
  }

  private sortByAgeDesc(user1: User, user2: User): number {
    if (user1.age < user2.age) {
      return 1; }
    if (user1.age > user2.age) {
      return -1; }
  }
}
