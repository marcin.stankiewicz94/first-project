package org.acme.internal;

import io.quarkus.test.junit.QuarkusTest;
import org.acme.domain.User;
import org.acme.internal.enums.Country;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

@QuarkusTest
class UserServiceTest {

    @Inject
    UserService userService;

    @Test
    void shouldReturnTrueWhenValidateCorrectUser(){
        // given
        User user = new User();
        user.setName("a");
        user.setSurname("a");
        user.setAge(20);
        user.setCountry(Country.POLAND);
        user.setEmail("arek@gmail.com");

        // when
        boolean result = userService.validateUser(user);

        // then
        assertTrue(result);
    }

    @Test
    void shouldReturnFalseWhenValidateWrongUser(){
        // given
        User user = new User();
        user.setName("a");
        user.setSurname("");
        user.setAge(20);
        user.setCountry(Country.POLAND);
        user.setEmail("a@a");

        // when
        boolean result = userService.validateUser(user);

        // then
        assertFalse(result);
    }

    @Test
    void shouldReturnUsersWithGivenParameterInNameOrSurname(){
        // given
        User user1 = new User();
        user1.setName("Marta");
        user1.setSurname("a");
        user1.setAge(20);
        user1.setCountry(Country.POLAND);
        user1.setEmail("marta@gmail.com");
        User user2 = new User();
        user2.setName("Arek");
        user2.setSurname("a");
        user2.setAge(20);
        user2.setCountry(Country.POLAND);
        user2.setEmail("arek@gmail.com");
        List<User> userList = new ArrayList<>();
        userList.add(user1);
        userList.add(user2);
        Stream<User> users = userList.stream();
        String param = "marta";

        // when
        List<User> usersWithGivenParameter = userService.findUsersWithGivenParameter(users, param);

        // then
        assertEquals(1, usersWithGivenParameter.size());
    }
}
