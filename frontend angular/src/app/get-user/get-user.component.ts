import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs/internal/Subscription';

@Component({
  selector: 'app-get-user',
  templateUrl: './get-user.component.html',
  styleUrls: ['./get-user.component.css']
})
export class GetUserComponent implements OnInit {

  addSubscription: Subscription;
  errorMessage = '';

  deleteUserForm = this.fb.group({
    id: ['']
  });

  constructor(private userService: UserService,
              private router: Router,
              private fb: FormBuilder) { }

  ngOnInit(): void {
  }

  onSubmit(value: { id: string; }): void {
    this.userService.getUser(value.id).subscribe(
      use => { this.router.navigate(['/quarkus/' + value.id]); },
      error => { this.errorMessage = 'Wrong ID! User with this ID does not exist.'; });
  }

  // tslint:disable-next-line:use-lifecycle-interface
  ngOnDestroy() {
    if (this.addSubscription){
      this.addSubscription.unsubscribe();
    }
  }
}
