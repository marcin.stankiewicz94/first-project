package org.acme.internal;

import io.quarkus.test.junit.QuarkusTest;
import org.acme.domain.User;
import org.acme.domain.UserRepository;
import org.acme.internal.enums.Country;
import org.junit.jupiter.api.*;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static io.restassured.RestAssured.given;

@QuarkusTest
class UserRestControllerTest {

    private String testEmail = "emailonlyintests@gmail.russia";
    private String testEmailToAdd = "emailonlyinaddtest@gmail.russia";

    @Inject
    private UserRepository userRepository;

    @Transactional
    @BeforeEach
    void addUser(){
        User user = new User();
        user.setName("test");
        user.setSurname("test");
        user.setAge(20);
        user.setCountry(Country.POLAND);
        user.setEmail(testEmail);
        userRepository.persist(user);
    }

    @Transactional
    @AfterEach
    void deleteUser(){
        userRepository.deleteByEmail(testEmail);
        userRepository.deleteByEmail(testEmailToAdd);
    }

    @Test
    void shouldCreateUser() {

        User user = new User();
        user.setName("test");
        user.setSurname("test");
        user.setAge(20);
        user.setCountry(Country.POLAND);
        user.setEmail(testEmailToAdd);
        String userLocation = given()
                .contentType(MediaType.APPLICATION_JSON)
                .body(user)
                .post("/users")
                .prettyPeek()
              .then()
                .statusCode(Response.Status.CREATED.getStatusCode())
                .extract().header(HttpHeaders.LOCATION);

        Assertions.assertNotNull(userLocation);

        User result = given()
                .contentType(MediaType.APPLICATION_JSON)
                .get(userLocation)
                .prettyPeek()
              .then()
                .statusCode(Response.Status.OK.getStatusCode())
                .extract().body().as(User.class);

        Assertions.assertNotNull(result);
        Assertions.assertTrue(userLocation.endsWith(result.getId()));
        Assertions.assertEquals(user.getName(), result.getName());
        Assertions.assertEquals(user.getSurname(), result.getSurname());
    }

    @Test
    void shouldReturnUserWhenFindUserById() {

        User createdUserInFirstTest = userRepository.findByEmail(testEmail);
        String id = createdUserInFirstTest.getId();
        io.restassured.response.Response response = given()
                .contentType(MediaType.APPLICATION_JSON)
                .get("/users/" + id)
                .prettyPeek()
              .then()
                .statusCode(Response.Status.OK.getStatusCode())
                .extract().response();

        Assertions.assertNotNull(response);

        String[] split = response.getBody().print().split(",");
        String resultLogin = split[8].split("\"")[3];
        String resultPassword = split[9].split("\"")[3];
        int resultAge = Integer.parseInt(split[10].split(":")[1]);
        String resultEmail = split[12].split("\"")[3];

        Assertions.assertEquals(createdUserInFirstTest.getName(), resultLogin);
        Assertions.assertEquals(createdUserInFirstTest.getSurname(), resultPassword);
        Assertions.assertEquals(createdUserInFirstTest.getAge(), resultAge);
        Assertions.assertEquals(createdUserInFirstTest.getEmail(), resultEmail);
    }

    @Test
    void shouldEditUser() {

        User user = new User();
        user.setName("editeduser");
        user.setSurname("editedpass");
        user.setAge(22);
        user.setCountry(Country.POLAND);
        user.setEmail(testEmail);

        User createdUserInFirstTest = userRepository.findByEmail(testEmail);
        String id = createdUserInFirstTest.getId();
        io.restassured.response.Response response = given()
                .contentType(MediaType.APPLICATION_JSON)
                .body(user)
                .put("/users/" + id)
                .prettyPeek()
              .then()
                .statusCode(Response.Status.OK.getStatusCode())
                .extract().response();

        Assertions.assertNotNull(response);

        String[] split = response.getBody().print().split(",");
        String resultLogin = split[8].split("\"")[3];
        String resultPassword = split[9].split("\"")[3];
        int resultAge = Integer.parseInt(split[10].split(":")[1]);
        String resultEmail = split[12].split("\"")[3];

        Assertions.assertEquals(user.getName(), resultLogin);
        Assertions.assertEquals(user.getSurname(), resultPassword);
        Assertions.assertEquals(user.getAge(), resultAge);
        Assertions.assertEquals(user.getEmail(), resultEmail);
    }

    @Test
    void shouldDeleteUser() {

        User createdUserInFirstTest = userRepository.findByEmail(testEmail);
        String id = createdUserInFirstTest.getId();
        String userLocation = given()
                .contentType(MediaType.APPLICATION_JSON)
                .delete("/users/" + id)
                .prettyPeek()
              .then()
                .statusCode(Response.Status.OK.getStatusCode())
                .extract().header(HttpHeaders.LOCATION);

        Assertions.assertNull(userLocation);
    }

    @Test
    void shouldReturnStatusNotFoundWhenFindUserByWrongId() {

        String id = "this-id-does-not-exist";
        given()
                .contentType(MediaType.APPLICATION_JSON)
                .get("/users/" + id)
                .prettyPeek()
              .then()
                .statusCode(Response.Status.NOT_FOUND.getStatusCode())
                .extract().response();
    }

    @Test
    void shouldReturnStatusNotFoundWhenEditUserByWrongId() {

        User user = new User();
        user.setName("editeduser");
        user.setSurname("editedpass");
        user.setAge(22);
        user.setCountry(Country.POLAND);
        user.setEmail("test@gmail.com");

        String id = "this-id-does-not-exist";
        given()
                .contentType(MediaType.APPLICATION_JSON)
                .body(user)
                .put("/users/" + id)
                .prettyPeek()
              .then()
                .statusCode(Response.Status.NOT_FOUND.getStatusCode())
                .extract().response();
    }

    @Test
    void shouldReturnStatusNotFoundWhenDeleteUserByWrongId() {

        String id = "this-id-does-not-exist";
        given()
                .contentType(MediaType.APPLICATION_JSON)
                .delete("/users/" + id)
                .prettyPeek()
              .then()
                .statusCode(Response.Status.NOT_FOUND.getStatusCode())
                .extract().response();
    }
}