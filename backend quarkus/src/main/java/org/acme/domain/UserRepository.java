package org.acme.domain;

import io.quarkus.hibernate.orm.panache.PanacheRepository;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class UserRepository implements PanacheRepository<User> {

    public User findByName(String name){
        return find("name", name).firstResult();
    }

    public User findByEmail(String email){
        return find("email", email).firstResult();
    }

    public void deleteByEmail(String email){
        delete("email", email);
    }
}
