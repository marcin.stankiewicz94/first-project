import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Routes, RouterModule } from '@angular/router';
import { AddUserComponent } from './add-user/add-user.component';
import { GetUserComponent } from './get-user/get-user.component';
import { DeleteUserComponent } from './delete-user/delete-user.component';
import { EditUserComponent } from './edit-user/edit-user.component';
import { HomepageComponent } from './homepage/homepage.component';
import { UserDetailsComponent } from './user-details/user-details.component';

export const routes: Routes = [
  { path: 'quarkus/admin', redirectTo: '/quarkus'},
  { path: 'quarkus', component: HomepageComponent},
  { path: 'quarkus/country/:country', component: HomepageComponent},
  { path: 'quarkus/search/:search', component: HomepageComponent},
  { path: 'quarkus/get', component: GetUserComponent},
  { path: 'quarkus/add', component: AddUserComponent},
  { path: 'quarkus/edit', component: EditUserComponent},
  { path: 'quarkus/edit/:id', component: EditUserComponent},
  { path: 'quarkus/delete', component: DeleteUserComponent},
  { path: 'quarkus/:id', component: UserDetailsComponent}
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
