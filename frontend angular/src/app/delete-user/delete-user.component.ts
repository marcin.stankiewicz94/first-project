import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { UserService } from '../services/user.service';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs/internal/Subscription';

@Component({
  selector: 'app-delete-user',
  templateUrl: './delete-user.component.html',
  styleUrls: ['./delete-user.component.css']
})
export class DeleteUserComponent implements OnInit {

  addSubscription: Subscription;
  errorMessage = '';

  deleteUserForm = this.fb.group({
    id: ['']
  });

  constructor(private userService: UserService,
              private router: Router,
              private fb: FormBuilder) { }

  ngOnInit(): void {
  }

  onSubmit(value: { id: string; }): void {
    this.userService.deleteUser(value.id).subscribe(
      user => {
        this.router.navigate(['/quarkus']);
        console.log('Deleted user: ' + value.id); },
      error => { this.errorMessage = 'Wrong ID! User with this ID does not exist.'; });
  }

  // tslint:disable-next-line:use-lifecycle-interface
  ngOnDestroy() {
    if (this.addSubscription){
      this.addSubscription.unsubscribe();
    }
  }
}
