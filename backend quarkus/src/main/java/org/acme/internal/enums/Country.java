package org.acme.internal.enums;

public enum Country {
    POLAND,
    GERMANY,
    FRANCE,
    OTHER
}
