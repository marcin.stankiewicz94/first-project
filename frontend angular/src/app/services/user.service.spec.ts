import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';

import { Observable, of } from 'rxjs';
import { UserService } from './user.service';
import { User } from '../user';
import { Country } from '../country';

fdescribe('UserService', () => {
  let service: UserService;
  let injector: TestBed;
  let httpClient: HttpTestingController;

  const API_URL = 'http://localhost:8080';

  const mockUsers = [
    {
      id: '1',
      name: 'aaa111',
      surname: 'aaa222',
      age: 20,
      country: Country.POLAND,
      email: 'aaa333',
    },
    {
      id: '2',
      name: 'bbb111',
      surname: 'bbb222',
      age: 20,
      country: Country.POLAND,
      email: 'bbb333',
    },
    {
      id: '3',
      name: 'ccc111',
      surname: 'ccc222',
      age: 20,
      country: Country.POLAND,
      email: 'ccc333',
    }
  ];

  const mockAddUser = {
      id: '4',
      name: 'ddd111',
      surname: 'ddd222',
      age: 20,
      country: Country.POLAND,
      email: 'ddd@333',
  };

  const mockEditUser = {
      id: '2',
      name: 'bbb111',
      surname: 'bbb222',
      age: 20,
      country: Country.POLAND,
      email: 'bbb333edited',
  };

  const mockDeleteUser = {
      id: '3',
      name: 'ccc111',
      surname: 'ccc222',
      age: 20,
      country: Country.POLAND,
      email: 'ccc333',
  };

  beforeEach(() => TestBed.configureTestingModule({
    schemas: [NO_ERRORS_SCHEMA],
    providers: [UserService],
    imports: [ReactiveFormsModule, RouterModule, HttpClientTestingModule, RouterTestingModule]
  }));

  beforeEach(() => {
    injector = getTestBed();
    httpClient = injector.get(HttpTestingController);
    service = injector.get(UserService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should use GET method', () => {
    service.getUsers().subscribe();
    const req = httpClient.expectOne(`${API_URL}/users`);
    expect(req.request.method).toBe('GET');
  });

  it('should use the right url in GET method', () => {
    service.getUsers().subscribe();
    const req = httpClient.expectOne(`${API_URL}/users`);
    expect(req.request.url).toBe(`${API_URL}/users`);
  });

  it('should return the right data in GET method', () => {
    service.getUsers().subscribe(data => {
      expect(data).toEqual(mockUsers);
    });
    const req = httpClient.expectOne(`${API_URL}/users`);
    req.flush(mockUsers);
  });

  it('should use POST method', () => {
    service.addUser(mockAddUser).subscribe();
    const req = httpClient.expectOne(`${API_URL}/users`);
    expect(req.request.method).toBe('POST');
  });

  it('should use the right url in POST method', () => {
    service.addUser(mockAddUser).subscribe();
    const req = httpClient.expectOne(`${API_URL}/users`);
    expect(req.request.url).toBe(`${API_URL}/users`);
  });

  it('should return the right data in POST method', () => {
    service.addUser(mockAddUser).subscribe(data => {
      expect(data).toEqual(mockAddUser);
    });
    const req = httpClient.expectOne(`${API_URL}/users`);
    req.flush(mockAddUser);
  });

  it('should use EDIT method', () => {
    service.editUser('2', mockEditUser).subscribe();
    const req = httpClient.expectOne(`${API_URL}/users/2`);
    expect(req.request.method).toBe('PUT');
  });

  it('should use the right url in EDIT method', () => {
    service.editUser('2', mockEditUser).subscribe();
    const req = httpClient.expectOne(`${API_URL}/users/2`);
    expect(req.request.url).toBe(`${API_URL}/users/2`);
  });

  it('should return edited user in EDIT method', () => {
    service.editUser('2', mockEditUser).subscribe(data => {
      expect(data).toEqual(mockEditUser);
    });
    const req = httpClient.expectOne(`${API_URL}/users/2`);
    req.flush(mockEditUser);
  });

  it('should use DELETE method', () => {
    service.deleteUser('3').subscribe();
    const req = httpClient.expectOne(`${API_URL}/users/3`);
    expect(req.request.method).toBe('DELETE');
  });

  it('should use the right url in DELETE method', () => {
    service.deleteUser('3').subscribe();
    const req = httpClient.expectOne(`${API_URL}/users/3`);
    expect(req.request.url).toBe(`${API_URL}/users/3`);
  });
});
