package org.acme.domain;

import lombok.Getter;
import lombok.Setter;
import org.acme.internal.enums.Country;
import org.tkit.quarkus.jpa.models.TraceableEntity;

import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@Table(name = "USERS")
public class User extends TraceableEntity {

    private String name;

    private String surname;

    private int age;

    private Country country;

    private String email;

    @Override
    public String toString() {
        return "User: " + name + " " + surname ;
    }
}
