package org.acme.internal;

import org.acme.domain.User;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.stream.Stream;

@Mapper
public interface UserMapper {

    UserDTO map(User user);

    Stream<UserDTO> map(Stream<User> userStream);

    @Mapping(target = "id", ignore = true)
    User create(UserDTO dto);
}
