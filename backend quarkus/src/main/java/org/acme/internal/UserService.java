package org.acme.internal;

import org.acme.domain.User;

import javax.enterprise.context.ApplicationScoped;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.acme.internal.validators.EmailValidator.isValidEmailAddress;

@ApplicationScoped
public class UserService {

    public boolean validateUser(User user) {
        return user.getName().length() > 0 && user.getSurname().length() > 0 &&
                user.getCountry() != null && isValidEmailAddress(user.getEmail());
    }

    public List<User> findUsersWithGivenParameter(Stream<User> user, String param){
        return user
                .filter(u -> u.getName().toLowerCase().startsWith(param.toLowerCase()) ||
                        u.getSurname().toLowerCase().startsWith(param.toLowerCase()))
                .collect(Collectors.toList());
    }
}
