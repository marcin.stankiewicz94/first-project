package org.acme.internal;

import org.acme.domain.User;
import org.acme.domain.UserDAO;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@ApplicationScoped
@Path("users")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class UserRestController {

    @Context
    UriInfo uriInfo;

    @Inject
    UserDAO userDAO;

    @Inject
    UserService userService;

    @POST
    public Response createUser(User user) {
        user.setId(UUID.randomUUID().toString());
        if (!userService.validateUser(user)) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        userDAO.create(user);
        return Response.created(uriInfo.getAbsolutePathBuilder().path(user.getId()).build())
                .entity(user)
                .build();
    }

    @GET
    public Response getUsers() {
        return Response.ok(userDAO.findAll()).build();
    }

    @GET
    @Path("{id}")
    public Response getUserById(@PathParam("id") String id) {
        User user = userDAO.findById(id);
        if (user == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        return Response.ok(user).build();
    }

    @PUT
    @Path("{id}")
    public Response updateUser(@PathParam("id") String id, User user) {
        User userToUpdate = userDAO.findById(id);
        if (userToUpdate == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        userToUpdate.setName(user.getName());
        userToUpdate.setSurname(user.getSurname());
        userToUpdate.setAge(user.getAge());
        userToUpdate.setCountry(user.getCountry());
        userToUpdate.setEmail(user.getEmail());
        userToUpdate = userDAO.update(userToUpdate);
        return Response.ok(userToUpdate).build();
    }

    @DELETE
    @Path("{id}")
    public Response deleteUser(@PathParam("id") String id) {
        User user = userDAO.findById(id);
        if (user == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        userDAO.deleteQueryById(id);
        return Response.ok().build();
    }

    @GET
    @Path("/find/{parameter}")
    public Response getUserByParameters(@PathParam("parameter") String param) {
        Stream<User> user = userDAO.findAll();
        if (user == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        List<User> collect = userService.findUsersWithGivenParameter(user, param);
        return Response.ok(collect).build();
    }
}
