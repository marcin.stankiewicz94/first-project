describe('My first test', () => {
    it('Does not do much!', () => {
      expect(true).to.equal(true)
    })
})

describe('My own tests', () => {
    it('Visits the main page', () => {
      cy.visit('http://localhost:4200/quarkus')
    })

    it('Clicks the link "GET USER"', () => {
      cy.visit('http://localhost:4200/quarkus')
      cy.contains('GET USER').click()
      cy.url().should('include', '/quarkus/get')
    })

    it('Clicks the link "ADD USER"', () => {
      cy.visit('http://localhost:4200/quarkus')
      cy.contains('ADD USER').click()
      cy.url().should('include', '/quarkus/add')
    })

    it('Clicks the link "EDIT USER"', () => {
      cy.visit('http://localhost:4200/quarkus')
      cy.contains('EDIT USER').click()
      cy.url().should('include', '/quarkus/edit')
    })

    it('Clicks the link "DELETE USER"', () => {
      cy.visit('http://localhost:4200/quarkus')
      cy.contains('DELETE USER').click()
      cy.url().should('include', '/quarkus/delete')
    })

    it('Should add new user', () => {
      cy.visit('http://localhost:4200/quarkus')
      cy.contains('ADD USER').click()
      cy.url().should('include', '/quarkus/add')

      cy.get('input#login').type('Arek')
      cy.get('input#password').type('Kaw')
      cy.get('input#age').type(28)
      cy.contains('POLAND').click()
      cy.get('input#email').type('arek.kaw@gmail.com')

    //   cy.get('button#addButton').click()
    })
})
