package org.acme.internal.enums;

public enum Gender {
    MAN,
    WOMAN,
    OTHER
}
