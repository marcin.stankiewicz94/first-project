package org.acme.internal;

import lombok.Getter;
import lombok.Setter;
import org.acme.internal.enums.Country;
import org.tkit.quarkus.rs.models.TraceableDTO;

@Getter
@Setter
public class UserDTO extends TraceableDTO {

    private String name;

    private String surname;

    private int age;

    private Country country;

    private String email;
}
