import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from '../services/user.service';
import { User } from '../user';
import { Country } from '../country';
import { Subscription } from 'rxjs/internal/Subscription';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent implements OnInit {

  addSubscription: Subscription;
  errorMessage = '';

  newUserForm = this.fb.group({
    login: [''],
    password: [''],
    age: [''],
    country: [''],
    email: ['']
  });

  constructor(private userService: UserService,
              private router: Router,
              private fb: FormBuilder) { }

  ngOnInit(): void {
  }

  onSubmit(value: { login: string; password: string; age: number; country: Country; email: string; }): void {
    const user = new User();
    user.name = value.login;
    user.surname = value.password;
    user.age = value.age;
    user.country = value.country;
    user.email = value.email;
    this.userService.addUser(user).subscribe(
      () => this.router.navigate(['/quarkus']),
      error => { this.errorMessage = 'Wrong email address.'; });
  }

  // tslint:disable-next-line:use-lifecycle-interface
  ngOnDestroy() {
    if (this.addSubscription){
      this.addSubscription.unsubscribe();
    }
  }
}
